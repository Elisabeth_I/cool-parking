﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.



using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Interfaces;
using System.Timers;


namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }
        System.Timers.Timer timer;

        public event ElapsedEventHandler Elapsed;

        public TimerService (double Interval)
        {
            this.Interval = Interval;
            timer = new System.Timers.Timer(Interval);
            timer.Elapsed += Elapsed;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }
        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}
