﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;
        private Parking parking = Parking.getInstance();
        private List<TransactionInfo> Transactions;
        public ParkingService (ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            Transactions = new List<TransactionInfo>();
            this.withdrawTimer = withdrawTimer;
            withdrawTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                for (int i = 0; i < parking.vehicles.Count; i++)
                { 
                    if (parking.vehicles[i].vehicleType == VehicleType.Truck)
                    {
                        if (parking.vehicles[i].Balance < 0)
                        {
                            parking.vehicles[i].Balance -= (Settings.penaltyRatio * Settings.tariffTruck);
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = (Settings.penaltyRatio * Settings.tariffTruck);
                            Transactions.Add(tr);
                            parking.parkingBalance += (Settings.penaltyRatio * Settings.tariffTruck);
                        }
                        else
                        if (parking.vehicles[i].Balance < Settings.tariffTruck)
                        {
                            parking.vehicles[i].Balance = -(Settings.tariffTruck - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = parking.vehicles[i].Balance + (Settings.tariffTruck - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            Transactions.Add(tr);
                            parking.parkingBalance += parking.vehicles[i].Balance + (Settings.tariffTruck - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                        }
                        else
                        {
                            parking.vehicles[i].Balance -= Settings.tariffTruck;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = Settings.tariffTruck;
                            Transactions.Add(tr);
                            parking.parkingBalance += Settings.tariffTruck;
                        }
                    }
                    
                    if (parking.vehicles[i].vehicleType == VehicleType.Bus)
                    {
                        if (parking.vehicles[i].Balance < 0)
                        {
                            parking.vehicles[i].Balance -= (Settings.penaltyRatio * Settings.tariffBus);
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = (Settings.penaltyRatio * Settings.tariffBus);
                            Transactions.Add(tr);
                            parking.parkingBalance += (Settings.penaltyRatio * Settings.tariffBus);
                        }
                        else 
                        if (parking.vehicles[i].Balance < Settings.tariffBus)
                        {
                            parking.vehicles[i].Balance = -(Settings.tariffBus - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = parking.vehicles[i].Balance + (Settings.tariffBus - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            Transactions.Add(tr);
                            parking.parkingBalance += parking.vehicles[i].Balance + (Settings.tariffBus - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                        } else
                        {
                            parking.vehicles[i].Balance = parking.vehicles[i].Balance - Settings.tariffBus;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = Settings.tariffBus;
                            Transactions.Add(tr);
                            parking.parkingBalance += Settings.tariffBus;
                        }
                    } else
                    if (parking.vehicles[i].vehicleType == VehicleType.Motorcycle)
                    {
                        if (parking.vehicles[i].Balance < 0)
                        {
                            parking.vehicles[i].Balance -= (Settings.penaltyRatio * Settings.tariffMotorcycle);
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = (Settings.penaltyRatio * Settings.tariffMotorcycle);
                            Transactions.Add(tr);
                            parking.parkingBalance += (Settings.penaltyRatio * Settings.tariffMotorcycle);
                        }
                        else
                        if (parking.vehicles[i].Balance < Settings.tariffMotorcycle)
                        {
                            parking.vehicles[i].Balance = -(Settings.tariffMotorcycle - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = parking.vehicles[i].Balance + (Settings.tariffMotorcycle - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            Transactions.Add(tr);
                            parking.parkingBalance += parking.vehicles[i].Balance + (Settings.tariffMotorcycle - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                        }
                        else
                        {
                            parking.vehicles[i].Balance = parking.vehicles[i].Balance - Settings.tariffMotorcycle;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = Settings.tariffMotorcycle;
                            Transactions.Add(tr);
                            parking.parkingBalance += Settings.tariffMotorcycle;
                        }
                    } else 
                    if (parking.vehicles[i].vehicleType == VehicleType.PassengerCar)
                    {
                        if (parking.vehicles[i].Balance < 0)
                        {
                            parking.vehicles[i].Balance -= (Settings.penaltyRatio * Settings.tariffPassengerCar);
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = (Settings.penaltyRatio * Settings.tariffPassengerCar);
                            Transactions.Add(tr);
                            parking.parkingBalance += (Settings.penaltyRatio * Settings.tariffPassengerCar);
                        }
                        else
                         if (parking.vehicles[i].Balance < Settings.tariffPassengerCar)
                        {
                            parking.vehicles[i].Balance = -(Settings.tariffPassengerCar - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = parking.vehicles[i].Balance + (Settings.tariffPassengerCar - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                            Transactions.Add(tr);
                            parking.parkingBalance += parking.vehicles[i].Balance + (Settings.tariffPassengerCar - parking.vehicles[i].Balance) * Settings.penaltyRatio;
                        }
                        else
                        {
                            parking.vehicles[i].Balance = parking.vehicles[i].Balance - Settings.tariffPassengerCar;
                            TransactionInfo tr = new TransactionInfo();
                            tr.time = DateTime.Now;
                            tr.Id = parking.vehicles[i].Id;
                            tr.Sum = Settings.tariffPassengerCar;
                            Transactions.Add(tr);
                            parking.parkingBalance += Settings.tariffPassengerCar;
                        }
                    }
                }
            };
            this.logTimer = logTimer;
            this.logService = logService;
            logTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                foreach (TransactionInfo transaction in Transactions)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(transaction.time.ToString());
                    sb.Append(transaction.Id);
                    sb.Append(transaction.Sum.ToString());
                    logService.Write(sb.ToString());
                    logService.Write("\n");
                }
                logService.Write("");
                Transactions = new List<TransactionInfo>();
            };
        }
        public void AddVehicle(Vehicle vehicle)
        {
                var collection = parking.vehicles.ToArray();
                var contains = (from u in collection where u.Id == vehicle.Id select u).ToArray();
                if (contains.Length > 0)
                {
                    throw new ArgumentException("Vehicle with this Id exists!");
                }
                parking.vehicles.Add(vehicle);  
        }

        public void Dispose()
        {
            
        }

        public decimal GetBalance()
        {
            return parking.parkingBalance;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.parkingCapacity - parking.vehicles.ToArray().Length;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var collection = parking.vehicles.ToArray();
            var contains = (from u in collection where u.Id == vehicleId select u).ToArray();
            if (contains.Length > 0)
            {
                
                    if (contains[0].Id == vehicleId)
                    {
                        if (contains[0].Balance < 0)
                        {
                            throw new ArgumentException("Balance must be >= 0!");
                        } else
                        {
                            parking.vehicles.Remove(contains[0]);
                        }
                    }
                
            } else throw new ArgumentException("Not exist this vehicle!");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) throw new ArgumentException("Sum must be > 0!");
            var collection = parking.vehicles.ToArray();
            var contains = (from u in collection where u.Id == vehicleId select u).ToArray();
            if (contains.Length > 0)
            {
                foreach (Vehicle vehicle in parking.vehicles)
                {
                    if (vehicle.Id == vehicleId)
                    {
                        vehicle.Balance += sum;
                    }
                }
            }
            else throw new ArgumentException("Not exist this vehicle!");
        }
    }
}
