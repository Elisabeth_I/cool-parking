﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.



using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logFilePath;
        public LogService(string logFilePath)
        {
            this.logFilePath = logFilePath;
           
        }
        public string LogPath { get; }

        public string Read()
        {
            
            string info = "";
            using (var file = new StreamReader(logFilePath))
            {
                info = file.ReadToEnd();
            }
            return info;
        }

        public void Write(string logInfo)
        {
            
            using (var file = new StreamWriter(logFilePath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}
