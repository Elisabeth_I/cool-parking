﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.



using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal startBalanceParking = 0;
        public static int parkingCapacity = 10;
        public static double billingPeriod = 5;
        public static double loggingPeriod = 60;
        public static decimal tariffPassengerCar = 2;
         public static decimal tariffTruck = 5M;
         public static decimal tariffBus = 3.5M;
        public static decimal tariffMotorcycle = 1;
        public static decimal penaltyRatio = 2.5M;
    }
}
