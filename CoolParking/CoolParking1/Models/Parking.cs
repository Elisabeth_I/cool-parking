﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal parkingBalance;
        public List<Vehicle> vehicles;
        private static Parking instance;

        private Parking()
        {
            vehicles = new List<Vehicle>();
            parkingBalance = Settings.startBalanceParking;
        }

        public static Parking getInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }
    }
}
