﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime time;
        public string Id;
        private decimal sum;
        public decimal Sum
        {
            get
            {
                return sum;
            }
            set
            {
                sum = value;
            }
        }
    }
}
