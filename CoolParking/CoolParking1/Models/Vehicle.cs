﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.


using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        readonly public string Id;
        readonly public VehicleType vehicleType;
        public decimal Balance;

        public Vehicle(string Id, VehicleType vehicleType, decimal Balance)
        {
           
                if (Balance < 0)
                {
                    throw new ArgumentException("Balance must be >= 0!");
                }
                if (Id.Length != 10)
                {
                    throw new ArgumentException("Id must be format ХХ-YYYY-XX, where Y - digit and X - letter in upper-case!");
                }
                if (Id[2] != '-' || Id[7] != '-')
                {
                    throw new ArgumentException("Id must be format ХХ-YYYY-XX, where Y - digit and X - letter in upper-case!");
                }
                for (int i = 3; i < 7; i++)
                {
                    if (!Char.IsDigit(Id[i]))
                    {
                        throw new ArgumentException("Id must be format ХХ-YYYY-XX, where Y - digit and X - letter in upper-case!");
                    }
                }
                for (int i = 0; i < 2; i++)
                {
                    if (!Char.IsLetter(Id[i]) || !Char.IsUpper(Id[i]))
                    {
                        throw new ArgumentException("Id must be format ХХ-YYYY-XX, where Y - digit and X - letter in upper-case!");
                    }
                }
                for (int i = 8; i < 10; i++)
                {
                    if (!Char.IsLetter(Id[i]) || !Char.IsUpper(Id[i]))
                    {
                        throw new ArgumentException("Id must be format ХХ-YYYY-XX, where Y - digit and X - letter in upper-case!");
                    }
                }
                this.Id = Id;
                this.vehicleType = vehicleType;
                this.Balance = Balance;
           
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder id = new StringBuilder();
            Random random = new Random();
            char c;
            c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            id.Append(c);
            c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            id.Append(c);
            id.Append('-');
            for (int i = 0; i < 4; i++)
            {
                c = Convert.ToChar(random.Next(0, 9));
                id.Append(c);
            }
            id.Append('-');
            c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            id.Append(c);
            c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            id.Append(c);
            return id.ToString();
        }
    }
}
