using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Controllers{

  
    [ApiController]
    public class ParkingController: ControllerBase
    {
        private ParkingServices parkingServices;
        public ParkingController(ParkingServices service)
        {
            parkingServices = service;
        }

        //GET api/parking/balance
        [Route("api/[controller]/balance")]
        [HttpGet]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(parkingServices.GetParkingBalance());
        }
        //GET api/parking/capacity
        [Route("api/[controller]/capacity")]
        [HttpGet]
        public ActionResult<int> GetCapacity()
        {
            return Ok(parkingServices.GetParkingCapacity());
        }
        
    }
}