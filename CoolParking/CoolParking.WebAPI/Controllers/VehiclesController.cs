using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Controllers{

  
    [ApiController]
    public class VehiclesController: ControllerBase
    {
        private ParkingServices parkingServices;
        public VehiclesController(ParkingServices service)
        {
            parkingServices = service;
        }

        //GET api/vehicles
        [Route("api/[controller]")]
        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            return Ok(parkingServices.GetVehicles());
        }
        
        
    }
}