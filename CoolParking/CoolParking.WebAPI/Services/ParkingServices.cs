using System.Collections.Generic;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class ParkingServices : IParkingServices
    {
        private HttpClient _client;
        public ParkingServices()
        {
            _client = new HttpClient();
        }

        static TimerService withdrawTimer = new TimerService(Settings.billingPeriod);
        static TimerService logTimer = new TimerService(Settings.loggingPeriod);
        static LogService logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
        ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);
       
        public decimal GetParkingBalance()
        {
        
            return JsonConvert.DeserializeObject<decimal> (parkingService.GetBalance().ToString());
        }

        public int GetParkingCapacity()
        {
            return JsonConvert.DeserializeObject<int> (parkingService.GetCapacity().ToString());
        }
        
        public List<Vehicle> GetVehicles()
        {
            return JsonConvert.DeserializeObject<List<Vehicle>>(parkingService.GetVehicles().ToString());
        }
    }

    public class Parking
    {
        
        [JsonProperty("id")]
        public int Id{ get; set; }
        [JsonProperty("balance")]
         public decimal Balance {get; set;}
        [JsonProperty("capacity")]
        public int Capacity {get; set; }
        [JsonProperty("freePlaces")]
        public int FreePlaces {get; set;}
        [JsonProperty("vehicles")]
        public List<Vehicle> Vehicle {get; set;}
    }

    public class Vehicle
    {
         [JsonProperty("id")]
        public string Id {get; set;}
         [JsonProperty("vehicleType")]
         public int VehicleType {get; set;}
          [JsonProperty("balance")]
          public decimal Balance {get; set;}
    }

    public class Transactions
    {
        [JsonProperty("vehicleId")]
        public string VehicleId {get; set;}
        [JsonProperty("sum")]
        public decimal Sum {get; set;}
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate {get; set;}
    }
}