using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IParkingServices
    {
        decimal GetParkingBalance();
        int GetParkingCapacity();
    }
}
