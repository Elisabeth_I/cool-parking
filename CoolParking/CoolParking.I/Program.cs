﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Linq;
using System.Timers;


namespace CoolParking.I
{
    class Program
    {
        static void Main(string[] args)
        {
            TimerService withdrawTimer = new TimerService(Settings.billingPeriod);
            TimerService logTimer = new TimerService(Settings.loggingPeriod);
            LogService logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            withdrawTimer.Start();
            logTimer.Start();
            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("Display the current Parking balance. - 1\n");
                Console.WriteLine("Display the amount of money earned for the current period (before writing to the log). - 2\n");
                Console.WriteLine("Display the number of available / occupied parking spaces. - 3\n");
                Console.WriteLine("Display all Parking Transactions for the current period (before writing to the log). - 4\n");
                Console.WriteLine("Display the transaction history (by reading the data from the Transactions.log file). - 5\n");
                Console.WriteLine("Display the list of Tr. funds located in the Parking lot. - 6\n");
                Console.WriteLine("Put vehicle to the Parking. - 7\n");
                Console.WriteLine("Pick up the vehicle from Parking. - 8\n");
                Console.WriteLine("Top up the balance of a specific vehicle. - 9\n");
                Console.WriteLine("If u want exit - 10\n");
                string k;
                k = Console.ReadLine();
                switch (k)
                {
                    case "1": Console.WriteLine(parkingService.GetBalance().ToString());
                        break;
                    case "2": var lastTransactions = parkingService.GetLastParkingTransactions();
                        Console.WriteLine(lastTransactions.Sum(tr => tr.Sum));
                        break;
                    case "3": Console.WriteLine(parkingService.GetFreePlaces().ToString());
                        Console.WriteLine((parkingService.GetCapacity() - parkingService.GetFreePlaces()).ToString());
                        break;
                    case "4":
                        var lastTransactionslog = parkingService.GetLastParkingTransactions();
                        foreach (TransactionInfo transactionInfo in lastTransactionslog)
                        {
                            Console.WriteLine($"Time: {transactionInfo.time}\n Id: {transactionInfo.Id}\n Sum: {transactionInfo.Sum}");
                        }
                        break;
                    case "5": Console.WriteLine(parkingService.ReadFromLog());
                        break;
                    case "6": var vehicles = parkingService.GetVehicles();
                        foreach (Vehicle vehicle in vehicles)
                        {
                            Console.WriteLine($"Id: {vehicle.Id}\n Type: {vehicle.vehicleType}\n Balance: {vehicle.Balance}");
                        }
                        break;
                    case "7": Console.WriteLine("Input Id vehicle XX-YYYY-XX: ");
                        string id = Console.ReadLine();
                        Console.WriteLine("Input vehicle type (0 - PassengerCar, 1 - Truck, 2 - Bus, 3 - Motorcycle): ");
                        int n = Convert.ToInt32(Console.ReadLine());
                        VehicleType vt = VehicleType.PassengerCar;
                        if (n == 0) vt = VehicleType.PassengerCar;
                        if (n == 1) vt = VehicleType.Truck;
                        if (n == 2) vt = VehicleType.Bus;
                        if (n == 3) vt = VehicleType.Motorcycle;
                        Console.WriteLine("Input balance: ");
                        int b = Convert.ToInt32(Console.ReadLine());
                        Vehicle v = new Vehicle(id, vt, b);
                        parkingService.AddVehicle(v);
                        break;
                    case "8":
                        Console.WriteLine("Input Id vehicle XX-YYYY-XX: ");
                         id = Console.ReadLine();
                        parkingService.RemoveVehicle(id);
                        break;
                    case "9":
                        Console.WriteLine("Input Id vehicle XX-YYYY-XX: ");
                        id = Console.ReadLine();
                        Console.WriteLine("Input sum: ");
                         b = Convert.ToInt32(Console.ReadLine());
                        parkingService.TopUpVehicle(id, b);
                        break;
                    case "10": exit = true; break;
                    default: Console.WriteLine("Incorrect input");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
